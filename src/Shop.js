import { useState, useEffect } from "react";
import "./App.css";
import {Link} from 'react-router-dom';

function Shop() {
  useEffect(() => {
    fetchItems();
  }, []);

  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const URL = "https://fortnite-api.theapinetwork.com/upcoming/get";
    const res = await fetch(URL).then((res) => res.json());
    setItems(res.data);
    console.log(res.data);
  };


  return (
    <div>
      {items &&
        items.map((item, index) => {
          return (
          <h1 key={index}>
            <Link to={`/shop/${item.itemId}`}>{item.item.name}</Link>
          </h1>);
        })}
    </div>
  );
}

export default Shop;