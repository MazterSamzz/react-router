import { useState, useEffect } from "react";
import "./App.css";

function Shop() {
  useEffect(() => {
    fetchItems();
  }, []);

  const [items, setItems] = useState([]);

  const fetchItems = async () => {
    const URL = "https://fortnite-api.theapinetwork.com/upcoming/get";
    const res = await fetch(URL).then((res) => res.json());
    setItems(res.data);
  };

  return (
    <div>
      {items &&
        items.map((item, index) => {
          console.log(item.item);
        })}
    </div>
  );
}

export default Shop;