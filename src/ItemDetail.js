import { useState, useEffect } from "react";
import "./App.css";
import {Link} from 'react-router-dom';

function ItemDetail({match}) {
  useEffect(() => {
    fetchItem();
    //console.log(match);
  }, []);

  const [item, setItem] = useState({
    images: {}
});

  const fetchItem = async () => {
    const URL = `https://fortnite-api.theapinetwork.com/item/get?id=${match.params.id}`;
    const res = await fetch(URL).then((res) => res.json());
    console.log(res.data.item);
    setItem(res.data.item);
  };

  return (
    <div>
        <h1>{item.name}</h1>
        <img src={item.images.information} alt="" />
    </div>
  );
}

export default ItemDetail;